package com.org.OrganizacionFinanciera.Repository;

import com.org.OrganizacionFinanciera.Entity.CuentaBancaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaBancariaRepository extends JpaRepository<CuentaBancaria, Long> {

    @Query("SELECT c FROM CuentaBancaria c WHERE c.nro_cuenta = :nro_cuenta")
    CuentaBancaria findByNumeroCuenta(@Param("nro_cuenta") Integer nro_cuenta);

}
