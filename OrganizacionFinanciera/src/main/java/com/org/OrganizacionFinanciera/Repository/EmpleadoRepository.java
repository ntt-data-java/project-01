package com.org.OrganizacionFinanciera.Repository;

import com.org.OrganizacionFinanciera.Entity.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {
}
