package com.org.OrganizacionFinanciera.Repository;

import com.org.OrganizacionFinanciera.Entity.Banco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends JpaRepository<Banco, Long> {

    // Listar Bancos por "nombre" usando Query nativa
    @Query(value = "SELECT * FROM banco WHERE nombre=?1", nativeQuery = true)
    List<Banco> listBanksWithNativeQuery(String nombre);

}
