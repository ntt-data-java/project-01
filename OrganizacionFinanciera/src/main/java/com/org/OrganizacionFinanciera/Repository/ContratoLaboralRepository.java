package com.org.OrganizacionFinanciera.Repository;

import com.org.OrganizacionFinanciera.Entity.ContratoLaboral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratoLaboralRepository extends JpaRepository<ContratoLaboral,Long> {

}
