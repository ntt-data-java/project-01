package com.org.OrganizacionFinanciera.Repository;

import com.org.OrganizacionFinanciera.Entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
