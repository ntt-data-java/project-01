package com.org.OrganizacionFinanciera.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BancoDto {

    private Long id;
    private String nombre;

}
