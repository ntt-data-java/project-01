package com.org.OrganizacionFinanciera.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmpleadoDto {

    private Long id;
    private String nombres;
    private String apellidos;
    private Long cuentaId;
    private Long contratoId;

}
