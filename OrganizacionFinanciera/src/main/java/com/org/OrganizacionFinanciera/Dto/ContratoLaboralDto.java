package com.org.OrganizacionFinanciera.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContratoLaboralDto {

    private Long id;
    private Double sueldo_base;
    private Integer tipo_contrato;

}
