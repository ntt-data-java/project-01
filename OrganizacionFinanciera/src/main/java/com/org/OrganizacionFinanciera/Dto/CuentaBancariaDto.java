package com.org.OrganizacionFinanciera.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancariaDto  {

    private Long id;
    private Integer nro_cuenta;
    private Long bancoId;

}
