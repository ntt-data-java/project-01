package com.org.OrganizacionFinanciera.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDto {

    private Long id;
    private String username;
    private String password;
    private Boolean active;
    private Long empleadoId;

}
