package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.UsuarioDto;
import com.org.OrganizacionFinanciera.Service.UsuarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioServiceImpl usuarioService;

    @GetMapping("/listar")
    public List<UsuarioDto> listar() {
        return usuarioService.listar();
    }

    @PostMapping("/registrar")
    public ResponseEntity<UsuarioDto> registrar(@RequestBody UsuarioDto usuarioDto) {
        return new ResponseEntity<>(usuarioService.registrar(usuarioDto), HttpStatus.CREATED);
    }

    @GetMapping("/buscar/{id}")
    public List<UsuarioDto> buscar(@PathVariable(value = "id") Long id) {
        return usuarioService.buscar(id);
    }

    @PutMapping("/actualizar/{id}")
    public UsuarioDto actualizar(@PathVariable Long id, @RequestBody UsuarioDto usuarioDto){
        return usuarioService.actualizar(id, usuarioDto);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar( @PathVariable(value = "id") Long id){
        usuarioService.eliminar(id);
        return new ResponseEntity<>("Elemento eliminado !!!", HttpStatus.OK);
    }

}
