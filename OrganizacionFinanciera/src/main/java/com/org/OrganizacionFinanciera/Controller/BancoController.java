package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.BancoDto;
import com.org.OrganizacionFinanciera.Service.IBancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/banco")
public class BancoController {

    @Autowired
    IBancoService iBancoService;

    @GetMapping("/listar")
    public List<BancoDto> listar() {
        return iBancoService.listar();
    }

    @PostMapping("/registrar")
    public BancoDto registrar(@RequestBody BancoDto bancoDto) {
        return iBancoService.registrar(bancoDto);
    }

    @PutMapping("/actualizar/{id}")
    public BancoDto actualizar(@PathVariable Long id,@RequestBody BancoDto bancoDto){
        return iBancoService.actualizar(id, bancoDto);
    }

    @GetMapping("/buscar/{id}")
    public BancoDto buscar(@PathVariable("id") long id){
        return iBancoService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id) {
        return iBancoService.eliminar(id);
    }

    @GetMapping("/filtro")
    public List<BancoDto> listarBancoPorNombre(@RequestParam String nombre) {
        return iBancoService.listarBancoPorNombre(nombre);
    }

}
