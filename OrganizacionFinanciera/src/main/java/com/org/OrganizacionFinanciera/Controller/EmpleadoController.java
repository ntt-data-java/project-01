package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.EmpleadoDto;
import com.org.OrganizacionFinanciera.Service.EmpleadoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/empleado")
public class EmpleadoController {

    @Autowired
    EmpleadoServiceImpl empleadoService;

    @PostMapping("/registrar")
    public ResponseEntity<EmpleadoDto> registrar(@RequestBody EmpleadoDto empleadoDto) {
        return new ResponseEntity<>(empleadoService.registrar(empleadoDto), HttpStatus.CREATED);
    }

    @GetMapping("/listar")
    public List<EmpleadoDto> listar() {
        return empleadoService.listar();
    }

    @PutMapping("/actualizar/{id}")
    public EmpleadoDto actualizar(@PathVariable Long id, @RequestBody EmpleadoDto empleadoDto){
        return empleadoService.actualizar(id, empleadoDto);
    }

    @GetMapping("/buscar/{id}")
    public List<EmpleadoDto> buscar(@PathVariable(value = "id") Long id) {
        return empleadoService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar( @PathVariable(value = "id") Long id){
        empleadoService.eliminar(id);
        return new ResponseEntity<>("Elemento eliminado !!!", HttpStatus.OK);
    }


}
