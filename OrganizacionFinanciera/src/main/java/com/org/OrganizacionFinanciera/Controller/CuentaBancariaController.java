package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.CuentaBancariaDto;
import com.org.OrganizacionFinanciera.Service.CuentaBancariaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cuentabancaria")
public class CuentaBancariaController {

    @Autowired
    CuentaBancariaServiceImpl cuentaBancariaService;

    @GetMapping("/listar")
    public List<CuentaBancariaDto> listar() {
        return cuentaBancariaService.listar();
    }

    @PostMapping("/registrar")
    public ResponseEntity<CuentaBancariaDto> registrar(@RequestBody CuentaBancariaDto cuentaBancariaDto) {
        return new ResponseEntity<>(cuentaBancariaService.registrar(cuentaBancariaDto), HttpStatus.CREATED);
    }

    @GetMapping("/buscar/{id}")
    public List<CuentaBancariaDto> buscar(@PathVariable(value = "id") Long id) {
        return cuentaBancariaService.buscar(id);
    }

    @PutMapping("/actualizar/{id}")
    public CuentaBancariaDto actualizar(@PathVariable Long id, @RequestBody CuentaBancariaDto cuentaBancariaDto){
        return cuentaBancariaService.actualizar(id, cuentaBancariaDto);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar( @PathVariable(value = "id") Long id){
        cuentaBancariaService.eliminar(id);
        return new ResponseEntity<>("Elemento eliminado !!!", HttpStatus.OK);
    }

    @GetMapping("/buscarPorNumero/{nro_cuenta}")
    public List<CuentaBancariaDto> buscarPorNumeroCuenta(@RequestParam Integer nro_cuenta) {
        return cuentaBancariaService.buscarPorNumeroCuenta(nro_cuenta);
    }



}
