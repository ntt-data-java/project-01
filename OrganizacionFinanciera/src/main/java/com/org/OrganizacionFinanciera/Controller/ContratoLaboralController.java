package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.ContratoLaboralDto;
import com.org.OrganizacionFinanciera.Service.ContratoLaboralServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contrato")
public class ContratoLaboralController {

    @Autowired
    ContratoLaboralServiceImpl contratoLaboralService;

    @GetMapping("/listar")
    public List<ContratoLaboralDto> listar() {
        return contratoLaboralService.listar();
    }

    @PostMapping("/registrar")
    public ContratoLaboralDto registrar(@RequestBody ContratoLaboralDto contratoLaboralDto) {
        return contratoLaboralService.registrar(contratoLaboralDto);
    }

    @PutMapping("/actualizar/{id}")
    public ContratoLaboralDto actualizar(@PathVariable Long id,@RequestBody ContratoLaboralDto contratoLaboralDto){
        return contratoLaboralService.actualizar(id, contratoLaboralDto);
    }

    @GetMapping("/buscar/{id}")
    public ContratoLaboralDto buscar(@PathVariable("id") long id){
        return contratoLaboralService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id) {
        return contratoLaboralService.eliminar(id);
    }
}
