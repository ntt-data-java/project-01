package com.org.OrganizacionFinanciera.Controller;

import com.org.OrganizacionFinanciera.Dto.RolDto;
import com.org.OrganizacionFinanciera.Service.RolServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/rol")
public class RolController {

    @Autowired
    RolServiceImpl rolService;

    @GetMapping("/listar")
    public List<RolDto> listar() {
        return rolService.listar();
    }

    @PostMapping("/registrar")
    public RolDto registrar(@RequestBody RolDto rolDto) {
        return rolService.registrar(rolDto);
    }

    @PutMapping("/actualizar/{id}")
    public RolDto actualizar(@PathVariable Long id,@RequestBody RolDto rolDto){
        return rolService.actualizar(id, rolDto);
    }

    @GetMapping("/buscar/{id}")
    public RolDto buscar(@PathVariable("id") long id){
        return rolService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id) {
        return rolService.eliminar(id);
    }

}
