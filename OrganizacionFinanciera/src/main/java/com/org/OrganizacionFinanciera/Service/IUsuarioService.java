package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.UsuarioDto;
import java.util.List;

public interface IUsuarioService {

    UsuarioDto registrar(UsuarioDto usuarioDto);
    List<UsuarioDto> buscar(Long id);
    List<UsuarioDto> listar();
    UsuarioDto actualizar(Long id, UsuarioDto usuarioDto);
    void eliminar(Long id);

}
