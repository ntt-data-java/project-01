package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.ContratoLaboralDto;
import com.org.OrganizacionFinanciera.Entity.ContratoLaboral;
import com.org.OrganizacionFinanciera.Repository.ContratoLaboralRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContratoLaboralServiceImpl implements IContratoLaboralService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ContratoLaboralRepository contratoLaboralRepository;


    @Override
    public ContratoLaboralDto registrar(ContratoLaboralDto contratoLaboralDto) {
        ContratoLaboral registrado = contratoLaboralRepository.save(mapearEntidad(contratoLaboralDto));
        return mapearDTO(registrado);
    }

    @Override
    public ContratoLaboralDto actualizar(Long id, ContratoLaboralDto contratoLaboralDto) {
        ContratoLaboral contrato = contratoLaboralRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        contrato.setTipo_contrato(contratoLaboralDto.getTipo_contrato());
        contrato.setSueldo_base(contratoLaboralDto.getSueldo_base());
        ContratoLaboral actualizado = contratoLaboralRepository.save(contrato);
        return mapearDTO(actualizado);
    }

    @Override
    public List<ContratoLaboralDto> listar() {
        List<ContratoLaboral> contratoLaborals = contratoLaboralRepository.findAll();
        return contratoLaborals.stream().map(contrato -> mapearDTO(contrato)).collect(Collectors.toList());
    }

    @Override
    public String eliminar(Long id) {
        ContratoLaboral contrato = contratoLaboralRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(contrato)) {
            contratoLaboralRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    @Override
    public ContratoLaboralDto buscar(Long id) {
        Optional<ContratoLaboral> found = contratoLaboralRepository.findById(id);
        return found.map(contratoLaboral ->mapearDTO(contratoLaboral)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private ContratoLaboral mapearEntidad(ContratoLaboralDto contratoLaboralDto){
        ContratoLaboral entidadMapeada = modelMapper.map(contratoLaboralDto, ContratoLaboral.class);
        return entidadMapeada;
    }
    private ContratoLaboralDto mapearDTO(ContratoLaboral contratoLaboral){
        ContratoLaboralDto DTOmapeado = modelMapper.map(contratoLaboral,ContratoLaboralDto.class);
        return DTOmapeado;
    }

}
