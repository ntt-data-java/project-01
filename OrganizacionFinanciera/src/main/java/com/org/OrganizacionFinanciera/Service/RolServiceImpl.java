package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.RolDto;
import com.org.OrganizacionFinanciera.Entity.Rol;
import com.org.OrganizacionFinanciera.Repository.RolRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RolServiceImpl implements IRolService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    RolRepository rolRepository;


    @Override
    public RolDto registrar(RolDto rolDto) {
        Rol registrado = rolRepository.save(mapearEntidad(rolDto));
        return mapearDTO(registrado);
    }

    @Override
    public RolDto actualizar(Long id, RolDto rolDto) {
        Rol rol = rolRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        rol.setNombre(rolDto.getNombre());
        Rol actualizado = rolRepository.save(rol);
        return mapearDTO(actualizado);
    }

    @Override
    public List<RolDto> listar() {
        List<Rol> rols = rolRepository.findAll();
        return rols.stream().map(valor -> mapearDTO(valor)).collect(Collectors.toList());
    }

    @Override
    public RolDto buscar(Long id) {
        Optional<Rol> rolFound = rolRepository.findById(id);
        return rolFound.map(rol -> mapearDTO(rol)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    @Override
    public String eliminar(Long id) {
        Rol rol = rolRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(rol)) {
            rolRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }


    /*************** MAPEO CON MODELMAPPER *******************/
    private Rol mapearEntidad(RolDto rolDto){
        Rol entidadMapeada = modelMapper.map(rolDto, Rol.class);
        return entidadMapeada;
    }
    private RolDto mapearDTO(Rol rol){
        RolDto DTOmapeado = modelMapper.map(rol,RolDto.class);
        return DTOmapeado;
    }

}
