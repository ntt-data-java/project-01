package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.BancoDto;

import java.util.List;

public interface IBancoService {

    BancoDto registrar(BancoDto bancoDto);
    BancoDto actualizar(Long id, BancoDto bancoDto);
    List<BancoDto> listar();
    String eliminar(Long id);
    BancoDto buscar(Long id);
    List<BancoDto> listarBancoPorNombre(String nombre);

}
