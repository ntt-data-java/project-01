package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.RolDto;
import java.util.List;

public interface IRolService {

    RolDto registrar(RolDto rolDto);
    RolDto actualizar(Long id, RolDto rolDto);
    List<RolDto> listar();
    String eliminar(Long id);
    RolDto buscar(Long id);

}
