package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.EmpleadoDto;
import com.org.OrganizacionFinanciera.Entity.ContratoLaboral;
import com.org.OrganizacionFinanciera.Entity.CuentaBancaria;
import com.org.OrganizacionFinanciera.Entity.Empleado;
import com.org.OrganizacionFinanciera.Repository.ContratoLaboralRepository;
import com.org.OrganizacionFinanciera.Repository.CuentaBancariaRepository;
import com.org.OrganizacionFinanciera.Repository.EmpleadoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {

    @Autowired
    CuentaBancariaRepository cuentaBancariaRepository;
    @Autowired
    ContratoLaboralRepository contratoLaboralRepository;
    @Autowired
    EmpleadoRepository empleadoRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public EmpleadoDto registrar(EmpleadoDto empleadoDto) {
        Empleado nuevoEmpleado = mapearEntidad(empleadoDto);
        Long cuentaId = empleadoDto.getCuentaId();
        Long contratoId = empleadoDto.getContratoId();
        if (cuentaId == null && contratoId == null) {
            throw new ResolutionException("ID no proporcionado");
        }
        CuentaBancaria cuentaBancaria = cuentaBancariaRepository.findById(cuentaId).orElseThrow(() -> new ResolutionException("Cuenta no encontrada"));
        ContratoLaboral contratoLaboral = contratoLaboralRepository.findById(contratoId).orElseThrow(() -> new ResolutionException("Contrato no encontrado"));
        nuevoEmpleado.setCuentaBancaria(cuentaBancaria);
        nuevoEmpleado.setContratoLaboral(contratoLaboral);
        Empleado empleado = empleadoRepository.save(nuevoEmpleado);
        return mapearDTO(empleado);
    }

    @Override
    public List<EmpleadoDto> buscar(Long id) {
        Optional<Empleado> empleados = empleadoRepository.findById(id);
        return empleados.stream().map(elemento -> mapearDTO(elemento)).collect(Collectors.toList());
    }


    @Override
    public List<EmpleadoDto> listar() {
        List<Empleado> empleados = empleadoRepository.findAll();
        return empleados.stream().map(empleado -> mapearDTO(empleado)).collect(Collectors.toList());
    }


    @Override
    public EmpleadoDto actualizar(Long id, EmpleadoDto empleadoDto) {
        Empleado empleado = empleadoRepository.findById(id).orElseThrow(() -> new RuntimeException("Empleado no presente"));
        empleado.setNombres(empleadoDto.getNombres());
        empleado.setApellidos(empleadoDto.getApellidos());
        if (empleadoDto.getCuentaId() != null){
            CuentaBancaria cuentaBancaria = cuentaBancariaRepository.findById(empleadoDto.getCuentaId()).orElseThrow(() -> new ResolutionException("Cuenta no encontrada"));
            empleado.setCuentaBancaria(cuentaBancaria);
        }
        if (empleadoDto.getContratoId() != null){
            ContratoLaboral contratoLaboral = contratoLaboralRepository.findById(empleadoDto.getContratoId()).orElseThrow(() -> new ResolutionException("Contrato no encontrado"));
            empleado.setContratoLaboral(contratoLaboral);
        }
        Empleado empleadoActualizado = empleadoRepository.save(empleado);
        return mapearDTO(empleadoActualizado);

    }


    @Override
    public void eliminar(Long id) {
        Empleado empleado = empleadoRepository.findById(id).orElseThrow(() -> new ResolutionException("Empleado no encontrado"));
        empleadoRepository.delete(empleado);

    }


    /*************** MAPEO CON MODELMAPPER *******************/
    private Empleado mapearEntidad(EmpleadoDto empleadoDto ){
        Empleado entidadMapeada = modelMapper.map(empleadoDto, Empleado.class);
        return entidadMapeada;
    }
    private EmpleadoDto mapearDTO(Empleado empleado){
        EmpleadoDto DTOmapeado = modelMapper.map(empleado,EmpleadoDto.class);
        return DTOmapeado;
    }
}
