package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.ContratoLaboralDto;

import java.util.List;

public interface IContratoLaboralService {

    ContratoLaboralDto registrar(ContratoLaboralDto contratoLaboralDto);
    ContratoLaboralDto actualizar(Long id, ContratoLaboralDto contratoLaboralDto);
    List<ContratoLaboralDto> listar();
    String eliminar(Long id);
    ContratoLaboralDto buscar(Long id);

}
