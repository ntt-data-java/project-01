package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.UsuarioDto;
import com.org.OrganizacionFinanciera.Entity.*;
import com.org.OrganizacionFinanciera.Repository.EmpleadoRepository;
import com.org.OrganizacionFinanciera.Repository.UsuarioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    EmpleadoRepository empleadoRepository;

    @Override
    public UsuarioDto registrar(UsuarioDto usuarioDto) {
        Usuario usuario = mapearEntidad(usuarioDto);
        Long empleadoId = usuarioDto.getEmpleadoId();
        if (empleadoId == null){
            throw new ResolutionException("ID de empleado no proporcionado");
        }
        Empleado empleado = empleadoRepository.findById(empleadoId).orElseThrow(() -> new ResolutionException("Empleado no encontrado"));
        usuario.setEmpleado(empleado);
        Usuario usuarioGuardado = usuarioRepository.save(usuario);
        return mapearDTO(usuarioGuardado);
    }

    @Override
    public List<UsuarioDto> buscar(Long id) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        return usuarioOptional.stream().map(user -> mapearDTO(user)).collect(Collectors.toList());
    }

    @Override
    public List<UsuarioDto> listar() {
        List<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios.stream().map(user -> mapearDTO(user)).collect(Collectors.toList());
    }

    @Override
    public UsuarioDto actualizar(Long id, UsuarioDto usuarioDto) {
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new ResolutionException("Usuario no encontrado"));
        usuario.setUsername(usuarioDto.getUsername());
        usuario.setPassword(usuarioDto.getPassword());
        usuario.setActive(usuarioDto.getActive());
        if (usuarioDto.getEmpleadoId() != null) {
            Empleado empleado = empleadoRepository.findById(usuarioDto.getEmpleadoId()).orElseThrow(() -> new ResolutionException("Empleado no encontrado"));
            usuario.setEmpleado(empleado);
        }
        Usuario usuarioActualizado = usuarioRepository.save(usuario);
        return mapearDTO(usuarioActualizado);
    }

    @Override
    public void eliminar(Long id) {
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new ResolutionException("Usuario no encontrado"));
        usuarioRepository.delete(usuario);

    }


    /*************** MAPEO CON MODELMAPPER *******************/
    private Usuario mapearEntidad(UsuarioDto usuarioDto){
        Usuario entidadMapeada = modelMapper.map(usuarioDto, Usuario.class);
        return entidadMapeada;
    }
    private UsuarioDto mapearDTO(Usuario usuario){
        UsuarioDto DTOmapeado = modelMapper.map(usuario,UsuarioDto.class);
        return DTOmapeado;
    }

}
