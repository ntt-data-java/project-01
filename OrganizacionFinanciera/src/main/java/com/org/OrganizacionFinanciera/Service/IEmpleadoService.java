package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.EmpleadoDto;

import java.util.List;

public interface IEmpleadoService {

    EmpleadoDto registrar(EmpleadoDto empleadoDto);
    List<EmpleadoDto> buscar(Long id);
    List<EmpleadoDto> listar();
    EmpleadoDto actualizar(Long id, EmpleadoDto empleadoDto);
    void eliminar(Long id);


}
