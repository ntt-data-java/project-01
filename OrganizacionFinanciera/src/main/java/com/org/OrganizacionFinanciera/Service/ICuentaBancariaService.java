package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.CuentaBancariaDto;

import java.util.List;

public interface ICuentaBancariaService {

    CuentaBancariaDto registrar(CuentaBancariaDto cuentaBancariaDto);
    List<CuentaBancariaDto> buscar(Long bancoId);
    List<CuentaBancariaDto> listar();
    CuentaBancariaDto actualizar(Long id, CuentaBancariaDto cuentaBancariaDto);
    void eliminar(Long id);

    List<CuentaBancariaDto> buscarPorNumeroCuenta(Integer nro_cuenta);

}
