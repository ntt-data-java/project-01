package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.CuentaBancariaDto;
import com.org.OrganizacionFinanciera.Entity.Banco;
import com.org.OrganizacionFinanciera.Entity.CuentaBancaria;
import com.org.OrganizacionFinanciera.Repository.BancoRepository;
import com.org.OrganizacionFinanciera.Repository.CuentaBancariaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CuentaBancariaServiceImpl implements ICuentaBancariaService {

    @Autowired
    CuentaBancariaRepository cuentaBancariaRepository;
    @Autowired
    BancoRepository bancoRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public CuentaBancariaDto registrar(CuentaBancariaDto cuentaBancariaDto) {
        CuentaBancaria nuevaCuenta = mapearEntidad(cuentaBancariaDto);
        Long bancoId = cuentaBancariaDto.getBancoId();
        if (bancoId == null) {
            throw new ResolutionException("ID de banco no proporcionado");
        }
        Banco banco = bancoRepository.findById(bancoId).orElseThrow(() -> new ResolutionException("Banco no encontrado"));
        nuevaCuenta.setBanco(banco);
        CuentaBancaria cuentaGuardada = cuentaBancariaRepository.save(nuevaCuenta);
        return mapearDTO(cuentaGuardada);
    }

    @Override
    public List<CuentaBancariaDto> buscar(Long bancoId) {
        Optional<CuentaBancaria> cuentaBancarias = cuentaBancariaRepository.findById(bancoId);
        return cuentaBancarias.stream().map(cuentaBancaria -> mapearDTO(cuentaBancaria)).collect(Collectors.toList());
    }

    @Override
    public List<CuentaBancariaDto> listar() {
        List<CuentaBancaria> cuentaBancarias = cuentaBancariaRepository.findAll();
        return cuentaBancarias.stream().map(cuenta -> mapearDTO(cuenta)).collect(Collectors.toList());
    }

    @Override
    public CuentaBancariaDto actualizar(Long id, CuentaBancariaDto cuentaBancariaDto) {
        CuentaBancaria cuentaBancaria = cuentaBancariaRepository.findById(id).orElseThrow(() -> new ResolutionException("Cuenta bancaria no encontrada"));
        if (cuentaBancariaDto.getNro_cuenta() != null) {
            cuentaBancaria.setNro_cuenta(cuentaBancariaDto.getNro_cuenta());
        }
        if (cuentaBancariaDto.getBancoId() != null) {
            Banco banco = bancoRepository.findById(cuentaBancariaDto.getBancoId()).orElseThrow(() -> new ResolutionException("Banco no encontrado"));
            cuentaBancaria.setBanco(banco);
        }
        CuentaBancaria cuentaActualizada = cuentaBancariaRepository.save(cuentaBancaria);
        return mapearDTO(cuentaActualizada);
    }

    @Override
    public void eliminar(Long id) {
        CuentaBancaria cuentaBancaria = cuentaBancariaRepository.findById(id).orElseThrow(() -> new ResolutionException("Cuenta bancaria no encontrada"));
        cuentaBancariaRepository.delete(cuentaBancaria);
    }

    @Override
    public List<CuentaBancariaDto> buscarPorNumeroCuenta(Integer nro_cuenta) {
        List<CuentaBancaria> cuentas = (List<CuentaBancaria>) cuentaBancariaRepository.findByNumeroCuenta(nro_cuenta);
        return cuentas.stream().map(this::mapearDTO).collect(Collectors.toList());
    }


    /*************** MAPEO CON MODELMAPPER *******************/
    private CuentaBancaria mapearEntidad(CuentaBancariaDto cuentaBancariaDto){
        CuentaBancaria entidadMapeada = modelMapper.map(cuentaBancariaDto, CuentaBancaria.class);
        return entidadMapeada;
    }
    private CuentaBancariaDto mapearDTO(CuentaBancaria cuentaBancaria){
        CuentaBancariaDto DTOmapeado = modelMapper.map(cuentaBancaria, CuentaBancariaDto.class);
        return DTOmapeado;
    }

}
