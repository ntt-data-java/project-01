package com.org.OrganizacionFinanciera.Service;

import com.org.OrganizacionFinanciera.Dto.BancoDto;
import com.org.OrganizacionFinanciera.Entity.Banco;
import com.org.OrganizacionFinanciera.Repository.BancoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BancoServiceImpl implements IBancoService {

    @Autowired
    BancoRepository bancoRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public BancoDto registrar(BancoDto bancoDto) {
        Banco registrado = bancoRepository.save(mapearEntidad(bancoDto));
        return mapearDTO(registrado);
    }

    @Override
    public BancoDto actualizar(Long id, BancoDto bancoDto) {
        Banco banco = bancoRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        banco.setNombre(bancoDto.getNombre());
        Banco actualizado = bancoRepository.save(banco);
        return mapearDTO(actualizado);
    }

    @Override
    public List<BancoDto> listar() {
        List<Banco> bancos = bancoRepository.findAll();
        return bancos.stream().map(banco -> mapearDTO(banco)).collect(Collectors.toList());
    }

    @Override
    public BancoDto buscar(Long id) {
        Optional<Banco> bancoFound = bancoRepository.findById(id);
        return bancoFound.map(banco ->mapearDTO(banco)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    @Override
    public List<BancoDto> listarBancoPorNombre(String nombre) {
        List<Banco> bancos = bancoRepository.listBanksWithNativeQuery(nombre);
        return bancos.stream().map(banco ->mapearDTO(banco)).collect(Collectors.toList());
    }

    @Override
    public String eliminar(Long id) {
        Banco banco = bancoRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(banco)) {
            bancoRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private Banco mapearEntidad(BancoDto bancoDto){
        Banco entidadMapeada = modelMapper.map(bancoDto, Banco.class);
        return entidadMapeada;
    }
    private BancoDto mapearDTO(Banco banco){
        BancoDto DTOmapeado = modelMapper.map(banco,BancoDto.class);
        return DTOmapeado;
    }

}
