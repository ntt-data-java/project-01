package com.org.OrganizacionFinanciera.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Empleado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombres;

    private String apellidos;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cuenta", referencedColumnName = "id")
    private CuentaBancaria cuentaBancaria;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", nullable = false)
    private ContratoLaboral contratoLaboral;

}
