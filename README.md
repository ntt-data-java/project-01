
# PROJECT 01

Proyecto en Java, con 6 entidades relaciononadas con JPA y operaciones CRUD, usa querys para consultas adicionales. Disponible documentacion de la API con Swagger UI y el archivo Postman para prueba de requests

## Tecnologias

**Language and Tool :** Java y Spring Boot

**Dependencies :** Spring Web, Spring Data JPA, Spring Boot DevTools, Lombok, MySQL Driver, ModelMapper, SpringDoc OpenAPI Starter WebMVC UI
